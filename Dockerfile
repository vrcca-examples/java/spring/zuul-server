FROM openjdk:8-jre-alpine

# folders
RUN ["mkdir", "-p", "/opt/app"]
WORKDIR /opt/app

# ports
EXPOSE 5555

# scripts
COPY scripts/entrypoint.sh entrypoint.sh
RUN ["chmod", "+x", "entrypoint.sh"]

ENTRYPOINT [ "sh", "entrypoint.sh" ]

# app executable
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar
