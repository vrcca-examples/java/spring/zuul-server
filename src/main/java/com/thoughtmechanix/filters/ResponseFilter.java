package com.thoughtmechanix.filters;

import brave.Tracer;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResponseFilter extends ZuulFilter {

    private static final int FILTER_ORDER = 1;
    public static final boolean SHOULD_FILTER = true;
    private Logger logger = LoggerFactory.getLogger(ResponseFilter.class);

    private final Tracer tracer;

    public ResponseFilter(Tracer tracer) {
        this.tracer = tracer;
    }

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        return SHOULD_FILTER;
    }

    @Override
    public Object run() {
        final RequestContext ctx = RequestContext.getCurrentContext();
        final String traceId = tracer.currentSpan().context().traceIdString();
        logger.debug("Adding the correlation id to the outbound headers. {}",
                traceId);
        ctx.getResponse().addHeader("tmx-correlation-id", traceId);
        logger.debug("Completing outgoing request for {}.", ctx.getRequest().getRequestURI());
        return null;
    }
}
